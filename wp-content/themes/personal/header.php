<?php
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Leet Digital
 * @since Leet Digital 2.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>T
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=10" />
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no, width=device-width">
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    <meta name="description" value="<?php bloginfo('description'); ?>"> 
    <meta name="keywords" content="Software Developer, Web Developer, Full-Stack
    Programmer, JavaScript Developer, Full-Stack JavaScript Developer, Senior Softw
    are Developer, Programming, Programmer, HTML, CSS, JavaScript, AngularJS, WordPr
    ess, Joomla, SQL, VB.net, Android Developer, Prynce Comiso, Prynce Junar Jake Co
    miso, Personal Portfolio, Personal Website, Portfolio, Web Development, Software
    Development, Full-Stack Software Development" />
    <meta name="google-site-verification" content="_VSoA4aYS-xkMaJudc5iZIJGWPUen
    8sPyK0djbwnXwY" />
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <link rel="shortcut icon" href="/favicon.png" type="image/x-icon">
    <link rel="icon" href="/favicon.png" type="image/x-icon">
    <!--[if lt IE 9]>
    <script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
    <![endif]-->
    <?php wp_head(); ?>
    <script type="text/paperscript" canvas="canvas" src="<?php echo get_template_directory_uri(); ?>/js/background.js"></script>
</head>

<body <?php body_class(); ?>>