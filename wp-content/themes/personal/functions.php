<?php

if ( ! function_exists( 'personal_setup' ) ) :

function personal_setup() {
    // Get language support
    // call _("text", "projectname") for auto translate feature
    load_theme_textdomain( 'personal_setup', get_template_directory() . '/languages' );

    // Enable support for Post Thumbnails, and declare two sizes.
    add_theme_support( 'post-thumbnails' );
    set_post_thumbnail_size( 672, 372, true );
    add_image_size( 'personal_setup-full-width', 1038, 576, true );

    // This theme uses wp_nav_menu() in two locations.
    register_nav_menus( array(
        'primary'   => __( 'Navigation', 'personal_setup' ),
    ) );

}
endif; 
add_action( 'after_setup_theme', 'personal_setup' );

function leetdigital_scripts() {
    // Load our main stylesheet.
    wp_enqueue_style( 'leetdigital-style', get_stylesheet_uri() );
    // Load your script
    wp_enqueue_script( 'leetdigital-script', get_template_directory_uri() . '/js/main.min.js', array( 'jquery' ));
    wp_enqueue_script( 'leetdigital-typed', get_template_directory_uri() . '/js/typed.min.js', array( 'jquery' ));
    wp_enqueue_script( 'leetdigital-paper', get_template_directory_uri() . '/js/paper-full.min.js', array( 'jquery' ));
    wp_enqueue_script( 'leetdigital-iscroll', get_template_directory_uri() . '/js/iscroll.js', array('jquery'));
    wp_enqueue_script( 'leetdigital-enquire', get_template_directory_uri() . '/js/enquire.min.js', array('jquery'));
    wp_localize_script( 'leetdigital-script', 'ajax_object',
        array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
}
add_action( 'wp_enqueue_scripts', 'leetdigital_scripts' );

/**
 * This is used to add custom header using wp-admin
 */
function leetdigital_wp_title( $title, $sep ) {
    global $paged, $page;

    if ( is_feed() ) {
        return $title;
    }

    // Add the site name.
    $title .= get_bloginfo( 'name', 'display' );

    // Add the site description for the home/front page.
    $site_description = get_bloginfo( 'description', 'display' );
    if ( $site_description && ( is_home() || is_front_page() ) ) {
        $title = "$title $sep $site_description";
    }

    // Add a page number if necessary.
    if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() ) {
        $title = "$title $sep " . sprintf( __( 'Page %s', 'leetdigital' ), max( $paged, $page ) );
    }

    return $title;
}
add_filter( 'wp_title', 'leetdigital_wp_title', 10, 2 );

function get_projects() {
    $company = $_POST['company'];
    $project = pods('project');
    $params  = array(
        'where' => 'company_relationship.slug = "' . $company . '"'
    );
    $project->find($params);
    $result = array();
    while ($project->fetch()) {
        $obj = array(
            'title' => $project->display('title'),
            'url'   => $project->display('project_url'),
            'description' => $project->display('project_description'),
            'screenshots' => $project->display('project_screenshots'),
            'technologies' => $project->display('technologies_used'),
            'icon' => $project->display('project_icon')
        );

        array_push($result, $obj);
    }

    echo json_encode($result);
    die();
}
add_action("wp_ajax_get_projects", "get_projects");
add_action("wp_ajax_nopriv_get_projects", "get_projects");

/*---*/

/*function junc_wp_default_styles($styles) {
   $styles->default_version = "20151016514";
   return $styles;
}
add_action("wp_default_styles", "junc_wp_default_styles");*/

/*----*/

// Remove issues with prefetching adding extra views
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0); 

// Implement Custom Header features.
require get_template_directory() . '/inc/custom-header.php';