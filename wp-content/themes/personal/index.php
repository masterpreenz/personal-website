<?php 
    get_header();
    $settings = pods('general_settings', array());
    $settings->fetch();
?>
<div class="main">
  <canvas class="background-canvas" id="canvas" resize></canvas>
  <div class="navigation-toggle">
    <div></div>
    <div></div>
    <div></div>
  </div>
  <div class="sidebar">
    <div class="logo">
      <img src="<?php echo $settings->display('logo'); ?>"/>
    </div>
    <ul class="nav-menu" id="nav-menu">
      <li class="menu-item active">
        <a href="#" id="home" data-view="close">HOME</a>
      </li>
      <li class="menu-item">
        <a href="#" id="about" data-view="about">ABOUT</a>
      </li>
      <li class="menu-item">
        <a href="#" id="skills" data-view="skills">MY SKILLS</a>
      </li>
      <li class="menu-item">
        <a href="#" id="work" data-view="work">WORK HISTORY</a>
      </li>
    </ul>
  </div>
  <div class="message">
    <span id="message"></span>
  </div>
  <div class="copyright">
    <p><?php echo $settings->display('copyright'); ?></p>
  </div>
  <div class="view" id="about">
    <div class="close" id="close">
      <div></div>
      <div></div>
    </div>
    <div class="wrapper">
      <div class="content">
        <div class="title">
          <h1>All about me</h1>
          <div class="sep"></div>
          <div class="social">
            <a class="icon-social" target="_blank" href="<?php echo $settings->display('facebook_url'); ?>">
              <i class="fa fa-facebook"></i>
            </a>
            <a class="icon-social" target="_blank" href="<?php echo $settings->display('linkedin_url');?>">
              <i class="fa fa-linkedin"></i>
            </a>
            <a class="icon-social" target="_blank" href="<?php echo $settings->display('stack_overflow_url'); ?>">
              <i class="fa fa-stack-overflow"></i>
            </a>
          </div>
        </div>
        <div class="bio iscroll show-scroll">
          <div class="container">
            <div class="details">
              <?php echo $settings->display('bio_details'); ?>
            </div>
            <div class="field-button">
              <a target="_blank" href="<?php echo $settings->display('cv'); ?>">Download my CV</a>
            </div>
            <div class="field">
              <label>Email</label>
              <a href="mailto:<?php echo $settings->display('email'); ?>"><?php echo $settings->display('email'); ?></a>
            </div>
            <?php
              $bio = pods('bio', array());
              while ($bio->fetch()) {
                $title = $bio->display('title');
                $value = $bio->display('value');
                ?>
                  <div class="field">
                    <label><?php echo $title; ?></label>
                    <p><?php echo $value; ?></p>
                  </div>
                <?php
              }
            ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="view" id="skills">
    <div class="close" id="close">
      <div></div>
      <div></div>
    </div>
    <div class="wrapper">
      <div class="content">
        <div class="title">
          <h1>My skills</h1>
          <div class="sep"></div>
        </div>
        <div class="skill-list iscroll">
          <div class="container">
            <?php
              $skill = pods('skill', array());
              while($skill->fetch()) {
                $title = $skill->display('title');
                $value = $skill->display('value');
                $bar   = 'bar-' . $value;
                $bar_color = $skill->display('bar_color');
                $background_position = $skill->display('background_position');
                ?>
                  <div class="field">
                    <div class="graph">
                      <h2 class="title"><?php echo $title; ?></h2>
                      <div class="value"><?php echo $value; ?></div>
                      <div class="bar <?php echo $bar; ?>" title="<?php echo $title; ?>" style="background: <?php echo $bar_color; ?>;"></div>
                    </div>
                    <div class="skill-icon" style="background-position-x: <?php echo $background_position; ?>"></div>
                  </div>
                <?php
              }
            ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="view" id="work">
    <div class="close" id="close">
      <div></div>
      <div></div>
    </div>
    <div class="wrapper">
      <div class="content">
        <div class="title">
          <h1>Work history</h1>
          <div class="sep"></div>
        </div>
        <p class="note">Click an item to view.</p>
        <div class="work-history iscroll">
          <div class="container">
            <?php
              $work = pods('work_history', array());
              while($work->fetch()) {
                $title = $work->display('title');
                $start_date = $work->display('start_date');
                $end_date = $work->display('end_date');
                $work_duration = $work->display('work_duration');
                $description = $work->display('work_description');
                $technologies = $work->display('technologies_used');
                $company_logo = $work->display('company_logo');
                $website = $work->display('company_website');
                $position = $work->display('position');
                $company_rel = $work->display('company');
                $company_rel 
                  = get_term_by('name', $company_rel, 'company')->slug;
                ?>
                  <div class="work-entry"
                       data-company-slug='<?php echo $company_rel; ?>'
                       data-description='<?php echo $description; ?>'
                       data-title="<?php echo $title; ?>"
                       data-website="<?php echo $website; ?>"
                       data-startdate="<?php echo $start_date; ?>"
                       data-enddate="<?php echo $end_date?>"
                       data-workduration="<?php echo $work_duration; ?>"
                       data-position="<?php echo $position; ?>"
                       data-companylogo="<?php echo $company_logo; ?>"
                       data-technologies="<?php echo $technologies; ?>">
                    <div class="opaque"></div>
                    <div class="background-image">
                      <img src="<?php echo $company_logo; ?>" />
                    </div>
                    <div class="heading">
                      <h2><?php echo $title; ?></h2>
                      <div class="duration">
                        <p class="duration-date"><?php echo $start_date; ?> to <?php echo $end_date; ?></p>
                        <p class="duration">(<?php echo $work_duration; ?>)</p>
                        <a class="website" href="<?php echo $website; ?>" target="_blank"><?php echo $website; ?></a>
                      </div>
                      <p class="position"><?php echo $position; ?></p>
                      <div class="more">
                        <div class="description">
                          <?php echo $description; ?>
                        </div>
                        <div class="technologies">
                          <?php
                            foreach($technologies as $item) {
                              ?>
                                <div class="item">
                                  <p><?php echo $item; ?></p>
                                </div>
                              <?php
                            }
                          ?>
                        </div>
                      </div>
                    </div>
                  </div>
                <?php
              }
            ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="overlay">
    <div class="close" id="close">
      <div></div>
      <div></div>
      <div></div>
    </div>
    <div class="overlay-view" id="work">
      <div class="heading">
        <div class="field">
          <h2 class="value title inline"></h2>
          <label>Company</label>
        </div>
        <div class="field">
          <a class="value website inline"></a>
          <label>Website</label>
        </div>
        <div class="field">
          <div class="value">
            <p class="date"></p><p class="duration"></p>
          </div>
          <label>Duration</label>
        </div>
        <div class="field">
          <p class="value position"></p>
          <label>Position</label>
        </div>
      </div>
      <div class="work-details">
        <div class="field">
          <div class="content">
          </div>
          <label>Details</label>
        </div>
      </div>
      <div class="sep"></div>
      <div class="projects">
        <label>Projects <p class="project-count"></p></label>
        <p class="note">Click to view project details</p>
        <div class="sk-circle loader">
          <div class="sk-circle1 sk-child"></div>
          <div class="sk-circle2 sk-child"></div>
          <div class="sk-circle3 sk-child"></div>
          <div class="sk-circle4 sk-child"></div>
          <div class="sk-circle5 sk-child"></div>
          <div class="sk-circle6 sk-child"></div>
          <div class="sk-circle7 sk-child"></div>
          <div class="sk-circle8 sk-child"></div>
          <div class="sk-circle9 sk-child"></div>
          <div class="sk-circle10 sk-child"></div>
          <div class="sk-circle11 sk-child"></div>
          <div class="sk-circle12 sk-child"></div>
        </div>
        <div class="list show-scroll">
          <div class="container">
          </div>
        </div>
      </div>
    </div>
    <div class="overlay-view hidden" id="project">
      <div class="heading">
        <div class="details">
          <div class="field">
            <h2 class="value title"></h2>
            <label>Project Name</label>
          </div>
          <div class="field">
            <a class="value website"></a>
            <label>Project Url</label>
          </div>
          <div class="field">
            <a class="value description"></a>
            <label>Description</label>
          </div>
          <div class="sep"></div>
          <div class="technologies">
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php 
    get_footer();
?>