jQuery(document).ready(function () {
  var overlay       = jQuery('.overlay'),
      visible       = false,
      data          = {},
      transitionOff = [
        'transitionend',
        'webkitTransitionEnd',
        'oTransitionEnd'
      ],
      isVisible     = function () {
        return visible;
      },
      onTransitionEnd   = function () {
        if (isVisible()) {
          jQuery(overlay).addClass('active');
        }
      },
      showOverlay    = function (event) {
        visible = true;
        jQuery(overlay).addClass('show');
      },
      hideOverlay    = function (event) {
        if (event !== undefined) {
          event.stopPropagation(); 
        }

        visible = false;
        jQuery(overlay).removeClass('active');
        jQuery(overlay).removeClass('show');
      },
      onCloseClicked = function (event) {
        event.stopPropagation();

        if (!jQuery(this).hasClass('back')) {
          hideOverlay();
        }
      },
      init           = function () {
        jQuery(document).on('showOverlay', showOverlay);
        jQuery(document).on('hideOverlay', hideOverlay);
        jQuery(overlay).find('#close').on('click', onCloseClicked);
        jQuery(overlay).on(transitionOff.join(' '), onTransitionEnd);
      };

  init();
});