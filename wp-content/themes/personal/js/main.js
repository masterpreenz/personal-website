//@koala-prepend 'message.js';
//@koala-prepend 'navigation.js';
//@koala-prepend 'about.js';
//@koala-prepend 'skills.js';
//@koala-prepend 'work.js';
//@koala-prepend 'overlay.js';

jQuery(document).ready(function () {
  var items   = jQuery('.iscroll');
      scrolls = [],
      initIScrolls = function () {
        jQuery.each(items, function (key, elem) {
          var fadeScrollbars = jQuery(this).hasClass('show-scroll') ? false : true;
          scrolls.push(new IScroll('.' + this.className.split(' ').join('.'), {
            'scrollbars': true,
            'fadeScrollbars': fadeScrollbars,
            'shrinkScrollbars': 'scale',
            'mouseWheel': true,
            'interactiveScrollbars': true
          }));
        });
      },
      fixAnchors = function () {
        var anchors = jQuery('a[href^="http://"]');

        jQuery(anchors).on('touchend', function (event) {
          window.open(jQuery(this).attr('href'), '_blank');
        });
      };
  initIScrolls();
  fixAnchors();

  var closeSidebar = function (event) {
        event.stopPropagation();

        if (jQuery('.sidebar').hasClass('active')) {
          jQuery('.sidebar').removeClass('active');
        }
      },
      showSidebar = function (event) {
        event.stopPropagation();

        jQuery('.sidebar').addClass('active');
      };

  enquire.register('screen and (max-width: 570px)', {
    match: function () {
      jQuery('.main').on('click', closeSidebar);
      jQuery('.sidebar .menu-item').on('click', closeSidebar);
      jQuery('.navigation-toggle').on('click', showSidebar);
    },
    unmatch: function () {
      jQuery('.main').off('click', closeSidebar);
      jQuery('.sidebar .menu-item').off('click', closeSidebar);
      jQuery('.navigation-toggle').off('click', showSidebar);
    }
  })
});