// The amount of symbol we want to place;
var STARCOUNT = 50;
var MAXSPEED  = 1.2;
var stars     = [];
var vector, mouseVector;
var Star      = function () {
    this.center  = Point.random() * view.size;
    this.angle   = Math.random() * 360;
    this.opacity = Math.random();
    this.fadeInc = Math.random() * 0.03;
    this.fadeFac = 1;
    this.draw    = function () {
        if (this.opacity > 1) {
            this.fadeFac = -1;
        } else if (this.opacity <= 0) {
            this.fadeFac = 1;
            this.center  = Point.random() * view.size;
            this.path.position = this.center;
        }

        this.opacity += this.fadeInc * this.fadeFac;
        this.opacity = this.opacity < 0 ? 0 : this.opacity;
        this.path.opacity = this.opacity;
    };
};
var initialize = function () {
    vector = new Point({
        angle: 45,
        length: 0
    });
    mouseVector = vector.clone();
    project.currentStyle = {
        fillColor: 'black'
    };

    for (var i = 0; i < STARCOUNT; i++) {
        var star   = new Star();
        star.scale = ((i / 3) + 1) / STARCOUNT;
        star.path  = new Shape.Circle(star.center, 5 * star.scale);
        star.path.opacity = star.opacity;
        // The center position is a random point in the view:
        star.path.data.vector = new Point({
            angle: star.angle,
            length : star.scale * Math.random() / 5
        });

        stars.push(star);
    }
};

// Place the instances of the symbol:
initialize();

function onMouseMove(event) {
    mouseVector = view.center - event.point;
}

// The onFrame function is called up to 60 times a second:
function onFrame(event) {
    vector = vector + (mouseVector - vector) / 30;

    // Run through the active layer's children list and change
    // the position of the placed symbols:
    for (var i = 0; i < stars.length; i++) {
        var item = project.activeLayer.children[i];
        var star = stars[i];
        var size = item.bounds.size;
        var length = vector.length / 30 * size.width / 30;
        item.position += vector.normalize(length) + item.data.vector;
        star.draw();
        keepInView(item);
    }
}

function keepInView(item) {
    var position = item.position;
    var itemBounds = item.bounds;
    var bounds = view.bounds;
    if (itemBounds.left > bounds.width) {
        position.x = -item.bounds.width;
    }

    if (position.x < -itemBounds.width) {
        position.x = bounds.width + itemBounds.width;
    }

    if (itemBounds.top > view.size.height) {
        position.y = -itemBounds.height;
    }

    if (position.y < -itemBounds.height) {
        position.y = bounds.height  + itemBounds.height / 2;
    }
}