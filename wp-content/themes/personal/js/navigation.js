jQuery(document).ready(function () {
  var nav      = jQuery('#nav-menu'),
      navItems = jQuery(nav).find('.menu-item'),
      clear    = function () {
        jQuery(navItems).removeClass('active');
      },
      onClick  = function (event) {
        event.stopPropagation();

        var self = jQuery(this).find('a'),
            id   = jQuery(self).attr('id'),
            view = jQuery(self).attr('data-view');

        jQuery(navItems).removeClass('active');
        jQuery(this).addClass('active');
        if (view == 'close') {
          jQuery(document).trigger('closeAll');
          jQuery(nav).find('#home').addClass('active');
        } else {
          jQuery(document).trigger('closeAll');
          jQuery(document).trigger('show', view);
        }
      },
      init = function () {
        jQuery(navItems).click(onClick)
      };

  init();
});