jQuery(document).ready(function () {
  var id         = 'work',
      view       = jQuery('.view#' + id),
      btnClose   = jQuery(view).find('#close'),
      visible    = false,
      projects   = {},
      items      = jQuery(view).find('.work-history .work-entry'),
      overlay    = jQuery('.overlay-view#work'),
      overlayProject = jQuery('.overlay-view#project'),
      overlayClose = jQuery('.overlay').find('#close'),
      projectMode = false,
      mobileMode = false,
      projectScroll,
      close      = function () {
        visible = false;
        jQuery(view).removeClass('active');
      },
      show       = function () {
        if (!visible) {
          visible = true;
          jQuery(view).addClass('active');
        }
      },
      onShow     = function (event, toView) {
        if (toView == id) {
          show();
        }
      },
      onClose    = function (event) {
        event.stopPropagation();
        close();
      },
      onCloseAll = function () {
        close();
      },
      UUID = function () {
        var s4 = function () {
          return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
        };

        return   s4() + s4() + '-' + s4() + '-' + s4() + '-' 
               + s4() + '-' + s4() + s4() + s4();
      },
      onTap = function (element, callback) {
        var tapStart = 0,
            callbackElement,
            touchStart = function (event) {
              tapStart = event.timeStamp;
              callbackElement = this;
            },
            touchEnd   = function (event) {
              var duration = parseInt(event.timeStamp - tapStart);
              if (duration <= 100) {
                callback(undefined, callbackElement);
              }
            },
            destroy = function () {
              jQuery(element).off('touchstart', touchStart);
              jQuery(element).off('touchend', touchEnd)
            },
            init = function () {
              jQuery(element).on('touchstart', touchStart);
              jQuery(element).on('touchend', touchEnd);
              jQuery(element).on('destroy', destroy);
            };

        init();
      },
      onItemClicked = function (event, element) {
        if (event !== undefined) {
          event.stopPropagation();
        }

        var obj  = {},
            elem = element === undefined ? this : element;
        obj['slug']          = jQuery(elem).attr('data-company-slug');
        obj['content']       = jQuery(elem).attr('data-description');
        obj['title']         = jQuery(elem).attr('data-title');
        obj['website']       = jQuery(elem).attr('data-website');
        obj['start_date']    = jQuery(elem).attr('data-startdate');
        obj['end_date']      = jQuery(elem).attr('data-enddate');
        obj['work_duration'] = jQuery(elem).attr('data-workduration');
        obj['position']      = jQuery(elem).attr('data-position');
        obj['company_logo']  = jQuery(elem).attr('data-companylogo');
        obj['technologies']  = jQuery(elem).attr('data-technologies');
        showInfo(obj);
      },
      projectClicked = function (event, element) {
        if (event !== undefined) {
          event.stopPropagation();
        }

        var elem = element === undefined ? this : element;
            id  = jQuery(elem).attr('id'),
            data = projects[id];

        projectMode = true;
        jQuery(overlayClose).addClass('back');
        jQuery(overlay).addClass('hidden');
        jQuery(overlayProject).removeClass('hidden');
        jQuery(overlayProject).find('.title').html(data['title']);
        jQuery(overlayProject)
          .find('.website')
          .attr('href', data['url'])
          .html(data['url'] == '#' ? 'No URL Available': data['url']);
        jQuery(overlayProject).find('.description').html(data['description']);

        var technologies = data['technologies'].split(',');
        var techTemplate = '';
        for (index in technologies) {
          var item = technologies[index];
          techTemplate += '<div class="item"><p>' + item + '</p></div>';
        }

        jQuery(overlayProject).find('.technologies').html(techTemplate);
        jQuery('.overlay').scrollTop(0);
      },
      showProjects = function (data) {
        projects = {};
        jQuery(overlay).find('.projects .list .project')
          .off('click touchstart', projectClicked);
        jQuery(overlay).find('.projects .list .project')
          .trigger('destroy');

        if (data.length == 0) {
          jQuery(overlay).find('.projects .project-count')
            .html('(' + 0 + ')');
          jQuery(overlay).find('.projects .list .container')
            .html('<p class="no-data">No Projects to Display</p>');
        } else {
          jQuery(overlay).find('.projects .project-count')
            .html('(' + data.length + ')');
          jQuery.each(data, function (key, project) {
            var template 
                  = '<div class="project" id="{id}">'
                  + ' <div class="image">'
                  + '  <div class="opaque"></div>'
                  + '   <img src="{image}" />'
                  + ' </div>'
                  + ' <div class="details">'
                  + '   <h2 class="title">{title}</h2>'
                  + '   <div class="technologies">'
                  + '     {technologies}'
                  + '   </div>'
                  + ' </div>'
                  + '</div>',
                id  = UUID(),
                obj = {
                  'title': project['title'],
                  'url'  : project['url'],
                  'description': project['description'],
                  'screenshots': project['screenshots'].split(' '),
                  'technologies': project['technologies'],
                  'icon': project['icon']
                };

            template = template.replace('{id}', id);
            template = template.replace('{image}', obj['screenshots'][0]);
            template = template.replace('{title}', obj['title']);
            var technologies = obj['technologies'].split(',');
            var techTemplate = '';
            for (index in technologies) {
              var item = technologies[index];
              techTemplate += '<div class="item"><p>' + item + '</p></div>';
            }

            template = template.replace('{technologies}', techTemplate);
            jQuery(overlay).find('.projects .list .container').append(template);
            projects[id] = obj;
          });
  
          if (!mobileMode) {
            jQuery(overlay).find('.projects .list .project')
              .on('click touchstart', projectClicked);
          } else {
            jQuery.each(jQuery(overlay).find('.projects .list .project'),
              function () {
                new onTap(jQuery(this), projectClicked);
              }
            );
          }
        }

        jQuery(overlay).find('.projects .list').removeClass('loading');
        jQuery(overlay).removeClass('loading');
        projectScroll.refresh();
      },
      getProjects = function (company) {
        jQuery(overlay).addClass('loading');
        jQuery(overlay).find('.projects .list .container').html('');
        jQuery(overlay).find('.projects .project-count').html('');
        jQuery.ajax({
          'url' : ajax_object.ajax_url,
          'type': 'POST',
          'data': {
            'action' : 'get_projects',
            'company': company
          }
        }).then(function (result) {
          showProjects(JSON.parse(result))
        });
      },
      showInfo   = function (data) {
        data['duration']      = data['start_date'] + ' to ' + data['end_date'];
        data['work_duration'] = '(' + data['work_duration'] + ')';
        data['technologies']  = data['technologies'].split(',');
        jQuery(overlay).find('.title').html(data['title']);
        jQuery(overlay)
          .find('.website')
          .attr('href', data['website'])
          .html(data['website']);
        jQuery(overlay).find('.position').html(data['position']);
        jQuery(overlay).find('.date').html(data['duration']);
        jQuery(overlay).find('.duration').html(data['work_duration']);
        jQuery(overlay).find('.content').html(data['content']);
        jQuery(document).trigger('showOverlay');
        getProjects(data['slug']);
      },
      onOverlayCloseClicked = function (event) {
        event.stopPropagation();
        event.stop = true;

        /* Make sure this event is the last one executed */
        setTimeout(function () {
          if (projectMode) {
            projectMode = false;
            jQuery(overlay).removeClass('hidden');
            jQuery(overlayProject).addClass('hidden');
            jQuery(overlayClose).removeClass('back');
          } else {
            jQuery(overlay).find('.projects .list .container').html('');
            jQuery(overlay).find('.projects .project-count').html('');
          }
        });
      },
      init          = function () {
        jQuery(overlayClose).on('click', onOverlayCloseClicked);
        jQuery(btnClose).on('click', onClose);
        jQuery(document).on('show', onShow);
        jQuery(document).on('closeAll', onCloseAll);
        jQuery.each(items, function () {
          jQuery(this).on('click touchstart', onItemClicked);
        });

        projectScroll = new IScroll('.overlay-view#work .projects .list', {
          'scrollbars': true,
          'fadeScrollbars': true,
          'shrinkScrollbars': 'scale'
        });
      };

  init();

  enquire.register('screen and (max-width: 470px)', {
    match: function () {
      mobileMode = true;

      jQuery.each(items, function () {
        jQuery(this).off('click touchstart', onItemClicked);
      });

      jQuery.each(items, function () {
        new onTap(jQuery(this), onItemClicked);
      });

      jQuery(overlayClose).on('touchstart', onOverlayCloseClicked);
      jQuery(btnClose).on('touchstart', onClose);
      jQuery(overlayClose).off('click', onOverlayCloseClicked);
      jQuery(btnClose).off('click', onClose);
    },
    unmatch: function () {
      mobileMode = false;

      jQuery.each(items, function () {
        jQuery(this).trigger('destroy');
      });

      jQuery(overlayClose).off('touchstart', onOverlayCloseClicked);
      jQuery(btnClose).off('touchstart', onClose);
      jQuery(overlayClose).on('click', onOverlayCloseClicked);
      jQuery(btnClose).on('click', onClose);
    },
    deferSetup: true
  });
});