jQuery(document).ready(function () {
  var id         = 'skills',
      view       = jQuery('.view#' + id),
      btnClose   = jQuery(view).find('#close'),
      visible    = false,
      close      = function () {
        visible = false;
        jQuery(view).removeClass('active');
      },
      show       = function () {
        if (!visible) {
          visible = true;
          jQuery(view).addClass('active');
        }
      },
      onShow     = function (event, toView) {
        if (toView == id) {
          show();
        }
      },
      onClose    = function (event) {
        event.stopPropagation();
        close();
      },
      onCloseAll = function () {
        close();
      },
      init       = function () {
        jQuery(btnClose).click(onClose);
        jQuery(document).on('show', onShow);
        jQuery(document).on('closeAll', onCloseAll);
      };

  init();
});