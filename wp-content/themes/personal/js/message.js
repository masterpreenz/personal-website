jQuery(document).ready(function () {
  var message = [
    '> Welcome to my domain',
    '> my name is <strong>Prynce</strong>',
    '> a professional <strong>Web Developer</strong>',
    '> or a <strong>Web Master</strong>',
    '> or <strong>Full-Stack Web Developer</strong>',
    '> OR whatever it\'s just titles',
    '> I know a little <i>Java for Android</i> too',
    '> also <i>Python</i>',
    '> and <i>LUA</i>',
    '> whatever. These are my skills: ',
    '> <i>HTML</i>, <i>CSS / CSS3</i>, <i>Javascript</i>, <i>AngularJS</i>, <i>Ionic</i>, <i>WordPress</i>, <i>Joomla!</i>, <i>PHP</i>',
    '> <i>VB.net</i>, <i>SQL</i>',
    '> also manipulation of <i>Linux</i> if that counts',
    '> and welcome to my personal portfolio',
    '> implementing a little of my knowledge for the best of this site',
    '> using <i>Javascript</i> and <i>CSS / CSS3</i>',
    '> and of course <i>HTML</i>',
    '> with the help of <i>WordPress</i> for dynamic content',
    '> showcasing all my work for all the past years',
    '> and companies I joined',
    '> and maybe if you wanted knowing more about me',
    '> by navigating other views',
    '> feel free to drop by in <strong>Contact</strong>',
    '> if you wanted to say hi!',
    '> or maybe working with me to my expertise',
    '> I hope you had fun looking at my masterpiece',
    '> Further improvement on this site is still on the way',
    '> Thank you'
  ];

  jQuery("#message").typed({
    strings: [
      message.join('^500 </br>')
    ],
    typeSpeed: 10
  });
});